<?php
/**
 * Plugin Name: IDFM Content
 * Description: Custom post type and taxonomies
 * Version: 1.0
 * Author: Netapsys
 */

add_action( 'init', 'add_projets' );
/*add_action( 'init', 'stif_common_rdv' );
add_action( 'init', 'links_post_type' );
add_action( 'init', 'add_category_taxonomy_to_rendez_vous' );
add_action( 'init', 'create_medias_cat_taxonomies', 0 );
add_action( 'init', 'stif_common_album' );
add_action( 'init', 'stif_common_faq' );
add_action( 'init', 'create_faqs_cat_taxonomies', 0 );
add_action( 'init', 'stif_common_acteurs' );
add_action( 'init', 'create_acteurs_cat_taxonomies', 0 );
add_action( 'admin_init', 'idfm_init_taxonomy' );
add_action( 'init', 'stif_common_avis' );
add_action( 'init', 'create_themes_cat_taxonomies', 5 );
add_action( 'init', 'add_themes' );
add_action( 'init', 'stif_common_creneaux' );


add_action( 'init', 'stif_common_decoupage' );*/

/*add_action( 'init', 'stif_common_ville' );
add_action( 'init', 'stif_common_secteur' );
*/

// Custom post type : Projets
function add_projets() {

	$labels = array(
		'name'               => _x( 'Projets', 'projet' ),
		'singular_name'      => _x( 'Projet', 'projet' ),
		'add_new'            => _x( 'Ajouter', 'projet' ),
		'add_new_item'       => _x( 'Ajouter un Projet', 'projet' ),
		'edit_item'          => _x( 'Editer un Projet', 'projet' ),
		'new_item'           => _x( 'Nouveau Projet', 'projet' ),
		'view_item'          => _x( 'Voir le Projet', 'projet' ),
		'search_items'       => _x( 'Rechercher un Projet', 'projet' ),
		'not_found'          => _x( 'Aucun Projet trouvé', 'projet' ),
		'not_found_in_trash' => _x( 'Aucun Projet dans la corbeille', 'projet' ),
		'parent_item_colon'  => _x( 'Projet parent :', 'projet' ),
		'menu_name'          => _x( 'Projet', 'projet' ),
	);

	$args = array(
		'labels'        => $labels,
		'hierarchical'  => false,
		'description'   => 'Les Projets du site.',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
		//'taxonomies' => array( 'category', 'faqs_tag' ),
		'public'        => true,
		'show_ui'       => true,
		'show_in_menu'  => true,
		'menu_position' => 4,

		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array('slug' => 'projet'),
		'capability_type'     => 'post'
	);

	register_post_type( 'projet', $args );
}





















// Custom post type : Rendez-vous
function stif_common_rdv() {

	$labels = array(
		'name'               => _x( 'Rendez-vous', 'rendez_vous' ),
		'singular_name'      => _x( 'Rendez-vous', 'rendez_vous' ),
		'add_new'            => _x( 'Ajouter', 'rendez_vous' ),
		'add_new_item'       => _x( 'Ajouter un Rendez-vous', 'rendez_vous' ),
		'edit_item'          => _x( 'Editer un Rendez-vous', 'rendez_vous' ),
		'new_item'           => _x( 'Nouveau Rendez-vous', 'rendez_vous' ),
		'view_item'          => _x( 'Voir le Rendez-vous', 'rendez_vous' ),
		'search_items'       => _x( 'Rechercher un Rendez-vous', 'rendez_vous' ),
		'not_found'          => _x( 'Aucun Rendez-vous trouvé', 'rendez_vous' ),
		'not_found_in_trash' => _x( 'Aucun Rendez-vous dans la corbeille', 'rendez_vous' ),
		'parent_item_colon'  => _x( 'Rendez-vous parent :', 'rendez_vous' ),
		'menu_name'          => _x( 'Rendez-vous', 'rendez_vous' ),
	);

	$args = array(
		'labels'        => $labels,
		'hierarchical'  => false,
		'description'   => 'Les Rendez-vous du site.',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
		//'taxonomies' => array( 'category', 'faqs_tag' ),
		'public'        => true,
		'show_ui'       => true,
		'show_in_menu'  => true,
		'menu_position' => 4,

		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array('slug' => 'rendez-vous'),
		'capability_type'     => 'post'
	);

	register_post_type( 'rendez_vous', $args );
}

//* Ajoute la categorie wordpress de base au custom post type rendez-vous

function add_category_taxonomy_to_rendez_vous() {
	register_taxonomy_for_object_type( 'category', 'rendez_vous' );
}

// Custom post type : Album

function stif_common_album() {

	$labels = array(
		'name'               => _x( 'Albums', 'albums' ),
		'singular_name'      => _x( 'Album', 'albums' ),
		'add_new'            => _x( 'Ajouter', 'albums' ),
		'add_new_item'       => _x( 'Ajouter un Album', 'albums' ),
		'edit_item'          => _x( 'Editer un Album', 'albums' ),
		'new_item'           => _x( 'Nouvel Album', 'albums' ),
		'view_item'          => _x( "Voir l'Album", 'albums' ),
		'search_items'       => _x( 'Rechercher un Album', 'albums' ),
		'not_found'          => _x( 'Aucun Album trouvé', 'albums' ),
		'not_found_in_trash' => _x( 'Aucun Album dans la corbeille', 'albums' ),
		'parent_item_colon'  => _x( 'Album parent :', 'albums' ),
		'menu_name'          => _x( 'Albums', 'albums' ),
	);

	$args = array(
		'labels'        => $labels,
		'hierarchical'  => false,
		'description'   => 'Les Albums du site.',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
		//'taxonomies' => array( 'category', 'faqs_tag' ),
		'public'        => true,
		'show_ui'       => true,
		'show_in_menu'  => true,
		'menu_position' => 8,

		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post'
	);

	register_post_type( 'albums', $args );
}

// Taxonomie : Médias


function create_medias_cat_taxonomies() {
	$labels = array(
		'name'              => _x( 'Catégories', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Catégorie', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Recherche de Catégories', 'textdomain' ),
		'all_items'         => __( 'Toutes les Catégories', 'textdomain' ),
		'parent_item'       => __( 'Catégorie Parent', 'textdomain' ),
		'parent_item_colon' => __( 'Catégorie Parent:', 'textdomain' ),
		'edit_item'         => __( 'Editer une Catégorie', 'textdomain' ),
		'update_item'       => __( "Mise à jour d'une Catégorie", 'textdomain' ),
		'add_new_item'      => __( 'Ajouter une nouvelle Catégorie', 'textdomain' ),
		'new_item_name'     => __( 'Nouveau nom de Catégorie', 'textdomain' ),
		'menu_name'         => __( 'Catégories', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'medias_cat' ),
	);

	register_taxonomy( 'medias_cat', array( 'attachment' ), $args );

}

// Custom post type : FAQ


function stif_common_faq() {

	$labels = array(
		'name'               => _x( 'FAQs', 'faqs' ),
		'singular_name'      => _x( 'FAQ', 'faqs' ),
		'add_new'            => _x( 'Ajouter', 'faqs' ),
		'add_new_item'       => _x( 'Ajouter une FAQ', 'faqs' ),
		'edit_item'          => _x( 'Editer une FAQ', 'faqs' ),
		'new_item'           => _x( 'Nouveau FAQ', 'faqs' ),
		'view_item'          => _x( 'Voir la FAQ', 'faqs' ),
		'search_items'       => _x( 'Rechercher une FAQ', 'faqs' ),
		'not_found'          => _x( 'Aucune FAQ trouvée', 'faqs' ),
		'not_found_in_trash' => _x( 'Aucune FAQ dans la corbeille', 'faqs' ),
		'parent_item_colon'  => _x( 'FAQ parent :', 'faqs' ),
		'menu_name'          => _x( 'FAQs', 'faqs' ),
	);

	$args = array(
		'labels'        => $labels,
		'hierarchical'  => false,
		'description'   => 'Les FAQs du site.',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
		//'taxonomies' => array( 'category', 'faqs_cat' ),
		'public'        => true,
		'show_ui'       => true,
		'show_in_menu'  => true,
		'menu_position' => 6,

		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post'
	);

	register_post_type( 'faqs', $args );
}

// Taxonomie : FAQ


function create_faqs_cat_taxonomies() {
	$labels = array(
		'name'              => _x( 'Catégories', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Catégorie', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Recherche de Catégories', 'textdomain' ),
		'all_items'         => __( 'Toutes les Catégories', 'textdomain' ),
		'parent_item'       => __( 'Catégorie Parent', 'textdomain' ),
		'parent_item_colon' => __( 'Catégorie Parent:', 'textdomain' ),
		'edit_item'         => __( 'Editer une Catégorie', 'textdomain' ),
		'update_item'       => __( "Mise à jour d'une Catégorie", 'textdomain' ),
		'add_new_item'      => __( 'Ajouter une nouvelle Catégorie', 'textdomain' ),
		'new_item_name'     => __( 'Nouveau nom de Catégorie', 'textdomain' ),
		'menu_name'         => __( 'Catégories', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'faqs_cat' ),
	);

	register_taxonomy( 'faqs_cat', array( 'faqs' ), $args );

}


function stif_common_decoupage() {
	$decoupage = get_option( 'IDFM_SITE_DECOUPAGE' );

	switch ( $decoupage ) {
		case 'villes':
			$labels = array(
				'name'               => _x( 'Villes', 'villes' ),
				'singular_name'      => _x( 'Ville', 'villes' ),
				'add_new'            => _x( 'Ajouter', 'villes' ),
				'add_new_item'       => _x( 'Ajouter une Ville', 'villes' ),
				'edit_item'          => _x( 'Editer une Ville', 'villes' ),
				'new_item'           => _x( 'Nouveau Ville', 'villes' ),
				'view_item'          => _x( 'Voir la Ville', 'villes' ),
				'search_items'       => _x( 'Rechercher une Ville', 'villes' ),
				'not_found'          => _x( 'Aucune Ville trouvée', 'villes' ),
				'not_found_in_trash' => _x( 'Aucune Ville dans la corbeille', 'villes' ),
				'parent_item_colon'  => _x( 'Ville parent :', 'villes' ),
				'menu_name'          => _x( 'Villes', 'villes' ),
			);

			$args = array(
				'labels'        => $labels,
				'hierarchical'  => false,
				'description'   => 'Les Villes du site.',
				'supports'      => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
				//'taxonomies' => array( 'category', 'faqs_cat' ),
				'public'        => true,
				'show_ui'       => true,
				'show_in_menu'  => true,
				'menu_position' => 5,

				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => true,
				'capability_type'     => 'post'
			);
			register_post_type( 'villes', $args );
			break;
		case 'secteurs':
			$labels = array(
				'name'               => _x( 'Secteurs', 'secteurs' ),
				'singular_name'      => _x( 'Secteur', 'secteurs' ),
				'add_new'            => _x( 'Ajouter', 'secteurs' ),
				'singular_name'      => _x( 'Secteur', 'secteurs' ),
				'singular_name'      => _x( 'Secteur', 'secteurs' ),
				'add_new_item'       => _x( 'Ajouter un Secteur', 'secteurs' ),
				'edit_item'          => _x( 'Editer un Secteur', 'secteurs' ),
				'new_item'           => _x( 'Nouveau Secteur', 'secteurs' ),
				'view_item'          => _x( 'Voir le Secteur', 'secteurs' ),
				'search_items'       => _x( 'Rechercher un Secteur', 'secteurs' ),
				'not_found'          => _x( 'Aucun Secteur trouvé', 'secteurs' ),
				'not_found_in_trash' => _x( 'Aucun Secteur dans la corbeille', 'secteurs' ),
				'parent_item_colon'  => _x( 'Secteur parent :', 'secteurs' ),
				'menu_name'          => _x( 'Secteurs', 'secteurs' ),
			);

			$args = array(
				'labels'              => $labels,
				'hierarchical'        => false,
				'description'         => 'Les Secteurs du site.',
				'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 5,
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => array('slug' => 'secteurs'),
				'capability_type'     => 'post'
			);
			register_post_type( 'villes', $args );
			break;
		default:
			break;
	}
	//var_dump($decoupage);
}


// Custom post type : Ville

/*
function stif_common_ville() {

	$labels = array(
		'name'               => _x( 'Villes', 'villes' ),
		'singular_name'      => _x( 'Ville', 'villes' ),
		'add_new'            => _x( 'Ajouter', 'villes' ),
		'add_new_item'       => _x( 'Ajouter une Ville', 'villes' ),
		'edit_item'          => _x( 'Editer une Ville', 'villes' ),
		'new_item'           => _x( 'Nouveau Ville', 'villes' ),
		'view_item'          => _x( 'Voir la Ville', 'villes' ),
		'search_items'       => _x( 'Rechercher une Ville', 'villes' ),
		'not_found'          => _x( 'Aucune Ville trouvée', 'villes' ),
		'not_found_in_trash' => _x( 'Aucune Ville dans la corbeille', 'villes' ),
		'parent_item_colon'  => _x( 'Ville parent :', 'villes' ),
		'menu_name'          => _x( 'Villes', 'villes' ),
	);

	$args = array(
		'labels'        => $labels,
		'hierarchical'  => false,
		'description'   => 'Les Villes du site.',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
		//'taxonomies' => array( 'category', 'faqs_cat' ),
		'public'        => true,
		'show_ui'       => true,
		'show_in_menu'  => true,
		'menu_position' => 5,

		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post'
	);

	register_post_type( 'villes', $args );
}

// Custom post type : Secteur


function stif_common_secteur() {

	$labels = array(
		'name'               => _x( 'Secteurs', 'secteurs' ),
		'singular_name'      => _x( 'Secteur', 'secteurs' ),
		'add_new'            => _x( 'Ajouter', 'secteurs' ),
		'singular_name'      => _x( 'Secteur', 'secteurs' ),
		'singular_name'      => _x( 'Secteur', 'secteurs' ),
		'add_new_item'       => _x( 'Ajouter un Secteur', 'secteurs' ),
		'edit_item'          => _x( 'Editer un Secteur', 'secteurs' ),
		'new_item'           => _x( 'Nouveau Secteur', 'secteurs' ),
		'view_item'          => _x( 'Voir le Secteur', 'secteurs' ),
		'search_items'       => _x( 'Rechercher un Secteur', 'secteurs' ),
		'not_found'          => _x( 'Aucun Secteur trouvé', 'secteurs' ),
		'not_found_in_trash' => _x( 'Aucun Secteur dans la corbeille', 'secteurs' ),
		'parent_item_colon'  => _x( 'Secteur parent :', 'secteurs' ),
		'menu_name'          => _x( 'Secteurs', 'secteurs' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'Les Secteurs du site.',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post'
	);

	register_post_type( 'secteurs', $args );
}
*/
// Custom post type : Les acteurs


function stif_common_acteurs() {

	$labels = array(
		'name'               => _x( 'Les acteurs', 'les_acteurs' ),
		'singular_name'      => _x( 'Les acteurs', 'les_acteurs' ),
		'add_new'            => _x( 'Ajouter', 'les_acteurs' ),
		'add_new_item'       => _x( 'Ajouter un acteur', 'les_acteurs' ),
		'edit_item'          => _x( 'Editer un acteur', 'les_acteurs' ),
		'new_item'           => _x( 'Nouvel acteur', 'les_acteurs' ),
		'view_item'          => _x( 'Voir l\'acteur', 'les_acteurs' ),
		'search_items'       => _x( 'Rechercher un acteur', 'les_acteurs' ),
		'not_found'          => _x( 'Aucun acteur trouvé', 'les_acteurs' ),
		'not_found_in_trash' => _x( 'Aucun acteur dans la corbeille', 'les_acteurs' ),
		'parent_item_colon'  => _x( 'Acteur parent :', 'les_acteurs' ),
		'menu_name'          => _x( 'Les acteurs', 'les_acteurs' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'Les acteurs du projet.',
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 7,
		'taxonomies'          => array( 'Groupe partenaires', 'acteurs_cat' ),
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post'
	);

	register_post_type( 'les_acteurs', $args );
}

// Taxonomie : Les acteurs


function create_acteurs_cat_taxonomies() {
	$labels = array(
		'name'              => _x( 'Groupe partenaires', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Groupe partenaires', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Recherche de groupe partenaires', 'textdomain' ),
		'all_items'         => __( 'Tous les groupes de partenaires', 'textdomain' ),
		'parent_item'       => __( 'Groupe partenaires parent', 'textdomain' ),
		'parent_item_colon' => __( 'Groupe partenaires Parent:', 'textdomain' ),
		'edit_item'         => __( 'Editer un groupe de partenaire', 'textdomain' ),
		'update_item'       => __( "Mise à jour d'un groupe de partenaire", 'textdomain' ),
		'add_new_item'      => __( 'Ajouter un nouveau groupe de partenaires', 'textdomain' ),
		'new_item_name'     => __( 'Nouveau nom de groupe de partenaires', 'textdomain' ),
		'menu_name'         => __( 'Groupe partenaires', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'acteurs_cat' ),
	);

	register_taxonomy( 'acteurs_cat', array( 'acteurs' ), $args );

}


function idfm_init_taxonomy() {
	$post_type = array( 'rendez_vous', 'albums' );

	foreach ( $post_type as $type ) {
		if ( post_type_exists( $type ) ) {
			register_taxonomy_for_object_type( 'category', $type );
			add_post_type_support( $type, 'category' );
		}
	}
}

// Custom post type : Avis


function stif_common_avis() {

	$labels = array(
		'name'               => _x( 'Avis', 'avis' ),
		'singular_name'      => _x( 'Avis', 'avis' ),
		'add_new'            => _x( 'Ajouter', 'avis' ),
		'add_new_item'       => _x( 'Ajouter un Avis', 'avis' ),
		'edit_item'          => _x( 'Editer un Avis', 'avis' ),
		'new_item'           => _x( 'Nouvel Avis', 'avis' ),
		'view_item'          => _x( 'Voir l\'Avis', 'avis' ),
		'search_items'       => _x( 'Rechercher un Avis', 'avis' ),
		'not_found'          => _x( 'Aucun Avis trouvé', 'avis' ),
		'not_found_in_trash' => _x( 'Aucun Avis dans la corbeille', 'avis' ),
		'parent_item_colon'  => _x( 'Avis parent :', 'avis' ),
		'menu_name'          => _x( 'Avis', 'avis' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'Les Avis du site.',
		'supports'            => array( 'title', 'custom-fields', 'revisions' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 8,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post'
	);

	register_post_type( 'avis', $args );
}

// Taxonomie : Thèmes

// Crée la taxonomy 'themes'
function create_themes_cat_taxonomies() {

	$labels = array(
		'name'              => _x( 'Thèmes', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Thème', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Recherche de Thème', 'textdomain' ),
		'all_items'         => __( 'Tous les Thèmes', 'textdomain' ),
		'parent_item'       => __( 'Thème Parent', 'textdomain' ),
		'parent_item_colon' => __( 'Thème Parent:', 'textdomain' ),
		'edit_item'         => __( 'Editer un Thème', 'textdomain' ),
		'update_item'       => __( "Mise à jour d'un Thème", 'textdomain' ),
		'add_new_item'      => __( 'Ajouter un nouveau Thème', 'textdomain' ),
		'new_item_name'     => __( 'Nouveau nom de Thème', 'textdomain' ),
		'menu_name'         => __( 'Thèmes', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => 'themes'
		),
	);

	register_taxonomy(
		'themes',
		'avis',
		$args
	);
}

// ajoute des termes à la catégorie 'themes'
function add_themes() {
	$themes = array(
		'stations-et-traces'         => 'Stations et Tracés',
		'caracteristiques-du-projet' => 'Caractéristiques du projet',
		'divers'                     => 'Divers'
	);

	foreach ( $themes as $key => $value ) {
		if ( ! term_exists( 'themes', $key ) ) {
			wp_insert_term(
				$value,
				'themes',
				array(
					'description' => '',
					'slug'        => $key
				)
			);
		}
	}
}


// Custom post type : Créneaux


function stif_common_creneaux() {

	$labels = array(
		'name'               => _x( 'Créneaux', 'creneaux' ),
		'singular_name'      => _x( 'Créneau', 'creneaux' ),
		'add_new'            => _x( 'Ajouter', 'creneaux' ),
		'add_new_item'       => _x( 'Ajouter un Créneau', 'creneaux' ),
		'edit_item'          => _x( 'Editer un Créneau', 'creneaux' ),
		'new_item'           => _x( 'Nouveau Créneau', 'creneaux' ),
		'view_item'          => _x( 'Voir le Créneau', 'creneaux' ),
		'search_items'       => _x( 'Rechercher un Créneau', 'creneaux' ),
		'not_found'          => _x( 'Aucun Créneau trouvé', 'creneaux' ),
		'not_found_in_trash' => _x( 'Aucun Créneau dans la corbeille', 'creneaux' ),
		'parent_item_colon'  => _x( 'Créneau parent :', 'creneaux' ),
		'menu_name'          => _x( 'Créneaux', 'creneaux' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'Les Créneaux du site.',
		'supports'            => array( 'title', 'custom-fields', 'revisions' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 9,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post'
	);

	register_post_type( 'creneaux', $args );
}


function links_post_type() {

	register_post_type( 'liens',
		array(
			'labels'             => array(
				'name'               => 'Liens',
				'singular_name'      => 'Lien',
				'add_new_item'       => 'Ajouter un lien',
				'edit_item'          => 'Editer un lien',
				'new_item'           => 'Nouveau lien',
				'view_item'          => 'Voir le lien',
				'search_items'       => 'Chercher',
				'not_found'          => 'Non trouvé',
				'not_found_in_trash' => 'Non trouvé dans la corbeille'
			),
			'capability_type'    => array( 'lien', 'liens' ),
			'rewrite'            => array( 'slug' => 'liens', 'with_front' => false ),
			'publicly_queryable' => true,
			'description'        => 'Ajoute des liens pour les affichages dans les dropdowns',
			'hierarchical'       => false,
			'menu_position'      => 5,
			'public'             => false,
			'show_in_admin_bar'  => false,
			'show_in_nav_menus'  => true,
			'show_ui'            => true,
			'supports'           => array( 'title' )
		) );
}


function na_remove_slug( $post_link, $post, $leavename ) {

	if ( 'liens' != $post->post_type || 'publish' != $post->post_status ) {
		return $post_link;
	}
	$post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

	return $post_link;
}

add_filter( 'post_type_link', 'na_remove_slug', 10, 3 );

function na_parse_request( $query ) {

	if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
		return;
	}
	if ( ! empty( $query->query['name'] ) || ! empty( $query->query['pagename'] ) ) {
		$query->set( 'post_type', array( 'liens', 'page', 'post' ) );
		if(empty($query->query['name'])){
			$query->set('name',$query->query['pagename']);
		}

	}}

add_action( 'pre_get_posts', 'na_parse_request' );


/*function create_links() {
	create_dynamical_link( 'actualite', 'Actualités', 'template-liste-des-actualites.php' );
	create_dynamical_link( 'rendez-vous', 'Rendez-vous', 'template-liste-des-rdv.php' );
	create_dynamical_link( 'mediatheque', 'Médiathèque', 'template-mediatheque.php' );
	\IdfmSiteFactory\create_dynamical_link('les-avis-de-la-concertation','Les avis de la concertation','template-liste-des-avis.php');
	\IdfmSiteFactory\create_dynamical_link('les-documents-de-la-concertation','Les documents de la concertation','template-liste-des-documents-concertation.php');
}

register_activation_hook( __FILE__, 'create_links' );*/


function getPostBySlug( $slug ) {

	$posttype = get_post_types();
	$posttype['secteurs'] = 'secteurs';
	$posttype['villes'] = 'villes';
	foreach ( $posttype as $pt ) {
		$content = get_page_by_title( $slug, OBJECT, $pt );
		if ( isset( $content ) ) {
			return $content;
		}
	}

	return null;

}


/*add_action( 'init', 'projet_rewrite_rule' );
add_action( 'template_redirect', 'prefix_url_rewrite_templates' );
add_filter( 'query_vars', 'prefix_register_query_var' );*/
/*
function projet_rewrite_rule() {

	//Actualités
	add_rewrite_rule( 'actualite(/*)$', 'index.php?post=all', 'top' );
	add_rewrite_rule( 'actualite/([^/]*)$', 'index.php?post=all&villes=$matches[1]', 'top' );

	//Médiathèque
	add_rewrite_rule( 'mediatheque(/*)$', 'index.php?mediatheque=all', 'top' );

	//Rendez-Vous
	add_rewrite_rule( 'rendez-vous(/*)$', 'index.php?rendezvous=all', 'top' );
	add_rewrite_rule( 'rendez-vous/([^/]*)$', 'index.php?rendezvous=all&villes=$matches[1]', 'top' );

	add_rewrite_rule( 'les-avis-de-la-concertation(/*)$', 'index.php?avis=all', 'top' );


}

function prefix_register_query_var( $vars ) {
	array_push( $vars, 'post' );
	array_push( $vars, 'mediatheque' );
	array_push( $vars, 'villes' );
	array_push( $vars, 'rendezvous' );
	array_push( $vars, 'avis' );
	return $vars;
}

// Fonction de redirection des templates
function prefix_url_rewrite_templates() {
	if ( get_query_var( 'post' ) ) {
		add_filter( 'template_include', function () {
			if ( $theme_file = locate_template( array( 'template-liste-des-actualites.php' ) ) ) {
				return $theme_file;
			} else {
				return null;
			}
		}
		);
		//add_filter( 'template_include', function($template_path) {return $template_path;});

	} elseif ( get_query_var( 'mediatheque' ) ) {
		add_filter( 'template_include', function () {
			if ( $theme_file = locate_template( array( 'template-mediatheque.php' ) ) ) {
				return $theme_file;
			} else {
				return null;
			}
		} );
	} elseif ( get_query_var( 'rendezvous' ) ) {
		add_filter( 'template_include', function () {
			if ( $theme_file = locate_template( array( 'template-liste-des-rdv.php' ) ) ) {
				return $theme_file;
			} else {
				return null;
			}
		} );
	} elseif ( get_query_var( 'avis' ) ) {
		add_filter( 'template_include', function () {
			if ( $theme_file = locate_template( array( 'template-liste-des-avis.php' ) ) ) {
				return $theme_file;
			} else {
				return null;
			}
		} );
	}

}*/