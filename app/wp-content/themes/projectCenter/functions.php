<?php

/*
 *  Ajout du JS et CSS dans le wp_head
 */
add_action( "wp_enqueue_scripts", "include_scripts" );

function include_scripts()
{
    // Ajout CSS du thème
    wp_enqueue_style("stylesheet_url", get_stylesheet_directory_uri() . "/style.css");

    // Ajout JS faq
    wp_enqueue_script( 'hideShow-js', get_stylesheet_directory_uri() . '/assets/js/hideShow.js', array( "jquery" ) );


    // Ajout CSS de Fancybox
    //wp_enqueue_style('fancybox-css', get_stylesheet_directory_uri() . '/assets/css/jquery.fancybox.min.css');

    // Ajout de fancybox
    //wp_enqueue_script('fancybox-js', get_stylesheet_directory_uri() . '/assets/js/jquery.fancybox.min.js', array("jquery"));
}

/*
 * Création des menus
 */
add_action( 'after_setup_theme', 'register_theme_menus' );
function register_theme_menus() {
    register_nav_menus(
        array(
            'menu_principal'     => __( 'Menu Principal' ),
            //'menu_footer_gauche' => __( 'Menu Footer gauche' ),
            //'menu_footer_droit'  => __( 'Menu Footer droit' ),
        )
    );
}
/*
 *  Permet l'ajout de classes aux items du mennu
 */
function nav_add_class_li( $classes, $item, $args ) {
    if ( 'menu_principal' === $args->theme_location ) {
        $classes[] = "col-2";
    }
    return $classes;
}
add_filter( 'nav_menu_css_class' , 'nav_add_class_li' , 10, 4 );

/*
 * Fonction qui récupère les données des projets au format ACF pour renvoyer ces données au format HTML
 */
function displayProjectContent($values)
{
    ?> <!-- <pre> <?php //var_dump($values);
?> </pre> --> <?
    if (!empty($values)) {
        $dataType = $values['type'];
        $dataValue = $values['value'];
        $dataLabel = $values['label'];
        $data = '';

        switch ($dataType) {
            case 'text':
            case 'textarea':
                $findHttp = 'http';
                $findWww = 'www';
                $posHttp = strpos($dataValue, $findHttp);
                $posWww = strpos($dataValue, $findWww);
                if (($posHttp === 0)||($posWww === 0)) {
                    $data = "<a href='$dataValue' target='_blank'>$dataValue</a>";
                } else {
                    $data = $dataValue;
                }
                break;
            case 'url':
                $data = "<a href='$dataValue' target='_blank'>$dataValue</a>";
                break;
            case 'file':
                if (!empty($dataValue)) {
                    $fileUrl = $dataValue['url'];
                    $fileName = $dataValue['name'];
                    $data = "<a href='$fileUrl'>$fileName</a>";
                } else {
                    $data = "Aucun fichier ajouté";
                }
                break;
            case 'user':
                if (!empty($dataValue)) {
                    //$data = "<ul>";
                    $cpt = 0;
                    foreach ($dataValue as $value) {
                        if($cpt == 0) {
                            $data .= $value['display_name'];
                        }
                        else{
                            $data .= ", " . $value['display_name'];
                        }
                        $cpt++;
                    }
                    //$data .= "</ul>";
                } else {
                    $data = "Aucun utilisateur renseigné";
                }
                break;
            default:
                $data = $dataValue;
        }
        $data = "<li class='row'><span class='col-3'>$dataLabel : </span><span class='col-9'>$data</span> </li>";
        return $data;
    }
}