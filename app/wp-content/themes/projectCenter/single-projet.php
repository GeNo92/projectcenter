<?php get_header(); ?>

<main role="main">
    <!-- section -->
    <section>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <div class="projectPage">

                <!-- post title -->
                <h2><?php the_title(); ?></h2>
                <!-- /post title -->

                <?php
                    // Get ACF Fields
                    $fields = get_field_objects();

                    if($fields){
                        // Tri des données
                        $fields_general = array();
                        $fields_local = array();
                        $fields_recette = array();
                        $fields_preprod = array();
                        $fields_production = array();

                        $flag_general = 0;
                        $flag_local = 0;
                        $flag_recette = 0;
                        $flag_preprod = 0;
                        $flag_production = 0;

                        foreach($fields as $key => $value) {
                            if (preg_match('/(general_)/',$key)){
                                $fields_general[$key] = $value;
                                if($flag_general == 0) $flag_general = 1;
                            }elseif (preg_match('/(local_)/',$key)){
                                $fields_local[$key] = $value;
                                if($flag_local == 0) $flag_local = 1;
                            }elseif (preg_match('/(recette_)/',$key)){
                                $fields_recette[$key] = $value;
                                if($flag_recette == 0) $flag_recette = 1;
                            }elseif (preg_match('/(preprod_)/',$key)){
                                $fields_preprod[$key] = $value;
                                if($flag_preprod == 0) $flag_preprod = 1;
                            }elseif (preg_match('/(production_)/',$key)){
                                $fields_production[$key] = $value;
                                if($flag_production == 0) $flag_production = 1;
                            }
                        }

                        // Création des différentes tableaux avec les données ordonnées pour simplifier l'affichage
                        $fullFlags = array($flag_general, $flag_local, $flag_recette, $flag_preprod, $flag_production);
                        $fullLabel = array('Général', 'Local', 'Recette', 'Preprod', 'Production');
                        $fullDatas = array($fields_general, $fields_local, $fields_recette, $fields_preprod, $fields_production );
                        $cptFlags = count($fullFlags);
                        ?>

                        <div class="container projectContainer">
                            <div class="row projectHeader">
                                <?php
                                    for ($i = 0; $i < $cptFlags; $i++){
                                        if($fullFlags[$i] == 1) {
                                            $classes = "data$i";
                                            if($i == "0") { $classes = "$classes firstLabel current"; }
                                            if($i == ($cptFlags - 1)) { $classes = "$classes lastLabel"; }
                                            echo "<div class='col labels $classes' id='label$i'>".$fullLabel[$i]."</div>";
                                        }
                                    }
                                ?>
                            </div>
                            <div class="row projectContent">
                                <?php
                                for ($i = 0; $i < count($fullFlags); $i++){
                                    if($fullFlags[$i] == 1) {
                                        $classes = "datas";
                                        if($i == "0") { $classes = "$classes"; }
                                        else{ $classes = "$classes hide"; }
                                        echo "<ul id='data$i' class='col $classes'>";
                                        foreach($fullDatas[$i] as $value){
                                            $data = displayProjectContent($value);
                                            echo $data;
                                        }
                                        echo "</ul>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    <?php
                    }
                ?>

            </div>
            <!-- /article -->

        <?php endwhile; ?>

        <?php else: ?>

            <!-- article -->
            <div>

                <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

            </div>
            <!-- /article -->

        <?php endif; ?>

    </section>
    <!-- /section -->
</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
