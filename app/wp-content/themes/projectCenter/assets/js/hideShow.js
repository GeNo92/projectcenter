(function ($) {
    $(document).ready(function() {
        /* Display or Hide tab project */
        $(".labels").click(function(e) {

            // Manage Labels
            var selectLabelId = $(this).attr('id');
            var getDataId = selectLabelId.replace("label", "data");
            if($("#"+selectLabelId).is(":not(.current)")){
                $(".labels").removeClass("current");
                $("#"+selectLabelId).addClass("current");
            }

            // Manage Datas
            if($("#"+getDataId).is(".hide")){
                $(".datas").addClass("hide");
                $("#"+getDataId).removeClass("hide");
            }
        });

    });
}(jQuery));