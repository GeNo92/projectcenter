<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

    <!--<link href="//www.google-analytics.com" rel="dns-prefetch"> -->
    <!--< <link href="<?php //echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon"> -->
    <!--<<link href="<?php //echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed"> -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <?php wp_head(); ?>
    <!-- <script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php // echo get_template_directory_uri(); ?>',
            tests: {}
        });
    </script> -->

</head>
<body <?php body_class(); ?>>

<!-- wrapper -->
<div class="wrapper">

    <div class="container">
        <div class="row">
            <div class="col no-padding">
                <!-- header -->
                <header class="header clear" role="banner">

                    <!-- title -->
                    <h1 class="generalTitle"><?php echo get_bloginfo( 'name' ); ?></h1>
                    <!-- /title -->

                    <!-- nav -->
<!--                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <nav class="header__menu_principal">
                                    <?php /*
                                   wp_nav_menu(
                                        array(
                                            'theme_location' => 'menu_principal'
                                        ) ) */
                                    ?>
                                </nav>
                            </div>
                        </div>
                    </div> -->

                    <?php
                    $defaults = array(
                    'theme_location'  => 'menu_principal',
                    'menu'            => '',
                    'container'       => 'nav',
                    'container_class' => 'container header__menu_principal',
                    'container_id'    => '',
                    'menu_class'      => 'menu',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s row">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => ''
                    );

                    wp_nav_menu( $defaults );
                    ?>
                    <!-- /nav -->

                </header>
                <!-- /header -->

