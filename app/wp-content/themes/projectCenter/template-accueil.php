<?php
/**
 * Template Name: Page d'accueil
 *
 */
?>

<?php get_header(); ?>

<main role="main">
    <!-- section -->
    <section>
        <!--
        // Texte présentation rapide
        // Liste projet select -> form
        // Mise à jour dynamique
        // affichage
        -->
        <div class="projectList">
            <div class="presentation">
                <h2>Bienvenue sur <?php echo bloginfo(); ?></h2>
                <p>Ce site à pour but principal de mettre à la disposition du visiteur les informations essentielles des
                    projets référencés. </p>
                <p>Prochainement il sera mis en place un système de mise en avant des fonctionnalités majeurs des projets.
                    Ceci permettra aux utilisateurs de récupérer rapidement certains éléments des projets pour les
                    réutiliser sur d'autres projets.</p>
            </div>
            <?php
            // The Query
            $args = array(
                'post_type' => 'projet',
                'post_status ' => 'publish',
                'order' => 'ASC',
                'orderby' => 'title'
            );
            $query = new WP_Query( $args );

            // The Loop
            if ( $query->have_posts() ) {
                echo "<p>Pour l'instant, voici la liste des projets présente sur ce site, vous pouvez donc sélectionner le projet
                que vous souhaitez afficher pour en voir le détail.</p>";
                echo '<div class="container liste">';
                    //echo '<div class="row">';
                    while ( $query->have_posts() ) {
                            $query->the_post();
                        echo '<div class="row"><a href="' . get_permalink() . '">' . get_the_title() . '</a></div>';
                        }
                    //echo '</div>';
                echo '</div>';
            } else {
                echo "<p>Pour l'instant aucun projet n'est référencé sur le site</p>";
            }
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>
        </div>

    </section>
    <!-- /section -->
</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
